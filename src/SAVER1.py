import csv
import random
import sys
from pathlib import Path

import tensorflow as tf  # noqa
from PIL import Image
from src.misc.generator import IMaterialistGenerator
from src.misc.utils import create_model_architecture  # noqa
from src.misc.utils import limit_gpu_memory_usage  # noqa
from src.misc.utils import set_memory_growth  # noqa
from src.models import unet
from src.components.image import IMaterialistImage
csv.field_size_limit(sys.maxsize)

resize = 1024
with open("kaggle_submission_TO_EDIT_EDITED.csv", "w") as outputcsv:
    fieldnames = ["ImageId", "EncodedPixels", "ClassId", "AttributesIds"]
    writer = csv.DictWriter(outputcsv, fieldnames=fieldnames)
    writer.writeheader()

    with open("kaggle_submission_TO_EDIT_TO_SAVE.csv", "r") as csvfile:
        fieldnames = ["ImageId", "EncodedPixels", "ClassId", "AttributesIds"]
        reader = csv.DictReader(csvfile, fieldnames=fieldnames)
        next(reader)

        for i, line in enumerate(reader):
            image_path = Path("test", f"{line['ImageId']}.jpg")
            with Image.open(image_path) as im:
                width, height = im.size

            if height > width:
                new_h = resize
                new_w = (resize * width) // height
            else:
                new_h = (resize * height) // width
                new_w = resize
            max_size = new_w * new_h

            split_encoded_pixels = line["EncodedPixels"].split()
            new_encoded_pixels = []
            for index, occurrences in IMaterialistImage._iter_encoded_pixels(split_encoded_pixels):
                if index + occurrences < max_size:
                    new_encoded_pixels.append((index + 1, occurrences + 1))

            # print(len(split_encoded_pixels))
            # print(len(new_encoded_pixels) // 2)

            reunited_encoded_pixels = " ".join([f"{x[0]} {x[1]}" for x in new_encoded_pixels])
            split_reunited_encoded_pixels = reunited_encoded_pixels.split()

            # outbound_ranges = len(split_encoded_pixels) - len(split_reunited_encoded_pixels)
            if split_reunited_encoded_pixels:
                writer.writerow({
                    "ImageId": line['ImageId'],
                    "EncodedPixels": reunited_encoded_pixels,
                    "ClassId": line['ClassId'],
                    "AttributesIds": line['AttributesIds'] # '"' + "".join(x for x in line['AttributesIds'] if int(x) < 300) + '"'
                })


s = set()

with Path("kaggle_submission_TO_EDIT_EDITED.csv").open() as csv_file:
    reader = csv.DictReader(csv_file)

    for i, line_dict in enumerate(reader):
        s.add(line_dict["ImageId"])

s2 = set()
for file in Path("test").iterdir():
    s2.add(file.name[:-len(".jpg")])

print(len(s), len(s2), len(s - s2), s == s2)

with open("kaggle_submission_TO_EDIT_EDITED.csv", "a") as csvfile:
    for image_id in sorted(s2 - s):
        fieldnames = ["ImageId", "EncodedPixels", "ClassId", "AttributesIds"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        r_p_s = random.randint(1, 50)
        r_p_o = random.randint(1, 5)
        class_id = random.randint(1, 40)

        writer.writerow({
            "ImageId": image_id,
            "EncodedPixels": f"{r_p_s} {r_p_o}",
            "ClassId": class_id,
            "AttributesIds": '10'
        })


s = set()

with Path("kaggle_submission_TO_EDIT_EDITED.csv").open() as csv_file:
    reader = csv.DictReader(csv_file)

    for i, line_dict in enumerate(reader):
        s.add(line_dict["ImageId"])

s2 = set()
for file in Path("test").iterdir():
    s2.add(file.name[:-len(".jpg")])
print(len(s), len(s2), s == s2)