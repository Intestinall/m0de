from typing import Tuple

from tensorflow.keras.activations import relu
from tensorflow.keras.layers import (
    Activation,
    BatchNormalization,
    Conv2D,
    Conv2DTranspose,
    Cropping2D,
    Dropout,
)


def dbnr(model, dropout_rate):
    """Add Dropout, BatchNormalization and ReLU activation layers."""
    model = Dropout(dropout_rate)(model)
    model = BatchNormalization()(model)
    return Activation(relu)(model)


def n_conv2d(model, n, dropout_rate, filters, padding="same"):
    """Add n conv2d."""
    for _ in range(n):
        model = Conv2D(
            filters=filters,
            kernel_size=3,
            padding=padding,
            kernel_initializer="he_normal",
        )(model)
        model = dbnr(model, dropout_rate)
    return model


def conv2d_transpose_crop(model, filters, strides=2, padding="valid", cropping=None):
    """Add a conv2d transpose layer and then crop it to keep the input size."""
    model = Conv2DTranspose(
        filters,
        kernel_size=3,
        strides=strides,
        padding=padding,
        kernel_initializer="he_normal",
    )(model)
    if cropping:
        return Cropping2D(cropping=cropping)(model)
    else:
        return model


def crop(dimi: int) -> Tuple[Tuple[int, int], Tuple[int, int]]:
    if dimi == 1:
        return (0, 1), (0, 1)
    elif dimi == 2:
        return (1, 1), (1, 1)
    else:
        raise NotImplementedError()
