from tensorflow.keras import Model
from tensorflow.keras.activations import sigmoid
from tensorflow.keras.layers import Activation, Conv2D, Input, MaxPooling2D, concatenate
from tensorflow.keras.losses import categorical_crossentropy, sparse_categorical_crossentropy
from tensorflow.keras.optimizers import Adam

from src.models.utils import conv2d_transpose_crop, crop, n_conv2d


def main(
    size: int,
    n_targets: int,
    dropout_rate: float = 0.2,
    base_filter: int = 64,
    lr: float = 1e-4,
    is_attribute: bool = False,
    serif: bool = False,
):
    """
    Sources :
        - https://heartbeat.fritz.ai/deep-learning-for-image-segmentation-u-net-architecture-ff17f6e4c1cf  # noqa
        - https://fr.wikipedia.org/wiki/U-Net
        - https://gist.github.com/hlamba28/6073e8ef011a1d47080f67522018d7e4#file-tgf_unet_model-py  # noqa
    """
    input_ = Input(shape=(size, size, 3))  # S

    # DOWNSCALING PHASE
    conv1 = n_conv2d(input_, 2, dropout_rate, filters=base_filter)  # S
    pool1 = MaxPooling2D(2)(conv1)  # S / 2

    conv2 = n_conv2d(pool1, 2, dropout_rate, filters=base_filter * 2)  # S / 2
    pool2 = MaxPooling2D(2)(conv2)  # S / 4

    conv3 = n_conv2d(pool2, 2, dropout_rate, filters=base_filter * 4)  # S / 4
    pool3 = MaxPooling2D(2)(conv3)  # S / 8

    conv4 = n_conv2d(pool3, 2, dropout_rate, filters=base_filter * 8)  # S / 8
    pool4 = MaxPooling2D(2)(conv4)  # S / 16

    conv5 = n_conv2d(pool4, 2, dropout_rate, filters=base_filter * 16)  # S / 16

    # UPSCALING PHASE
    up1 = conv2d_transpose_crop(conv5, base_filter * 16, cropping=crop(1))  # S / 8
    add_4 = concatenate([up1, conv4])  # S / 8
    up_conv1 = n_conv2d(add_4, 2, dropout_rate, filters=base_filter * 8)  # S / 8

    up2 = conv2d_transpose_crop(up_conv1, base_filter * 8, cropping=crop(1))  # S / 4
    add_3 = concatenate([up2, conv3])  # S / 4
    up_conv2 = n_conv2d(add_3, 2, dropout_rate, filters=base_filter * 4)  # S / 4

    up3 = conv2d_transpose_crop(up_conv2, base_filter * 4, cropping=crop(1))  # S / 2
    add_2 = concatenate([up3, conv2])  # S / 2
    up_conv3 = n_conv2d(add_2, 2, dropout_rate, filters=base_filter * 2)  # S / 2

    up4 = conv2d_transpose_crop(up_conv3, base_filter * 2, cropping=crop(1))  # S
    add_1 = concatenate([up4, conv1])  # S
    up_conv4 = n_conv2d(add_1, 2, dropout_rate, filters=base_filter)  # S

    # SCORE PHASE
    if serif:
        score = conv2d_transpose_crop(up_conv4, n_targets, padding="same")
    else:
        score = Conv2D(n_targets, kernel_size=3, padding="same")(up_conv4)

    predictions = Activation(sigmoid)(score)
    model = Model(inputs=input_, outputs=predictions)
    if is_attribute:
        model.compile(
            optimizer=Adam(lr=lr), loss=categorical_crossentropy, metrics=["accuracy"],
        )
    else:
        model.compile(
            optimizer=Adam(lr=lr),
            loss=sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )
    return model
