# from functools import partial
#
# from tensorflow.keras import Model
# from tensorflow.keras.activations import softmax
# from tensorflow.keras.layers import Add, Conv2D, Input, MaxPooling2D, Activation
# from tensorflow.keras.losses import sparse_categorical_crossentropy
# from tensorflow.keras.metrics import sparse_categorical_accuracy
# from tensorflow.keras.optimizers import Adam
#
# from src.models.utils import conv2d_transpose_crop, n_conv2d, dbnr
#
#
# def main(
#     size: int,
#     n_classes: int,
#     dropout_rate: float = 0.4,
#     lr: float = 10 ** -4,
#     filters_multipliers: float = 0.0,
# ):
#     filters = partial(lambda m, n: int(n * m), filters_multipliers)
#     cropping = ((1, 0), (1, 0))
#     input_ = Input(shape=(size, size, 3))  # S
#
#     conv1 = n_conv2d(input_, 2, dropout_rate, padding="same", filters=filters(64)) # S
#     pool1 = MaxPooling2D(2)(conv1)  # S / 2
#
#     conv2 = n_conv2d(
#         pool1, 2, dropout_rate, padding="same", filters=filters(128)
#     )  # S / 2
#     pool2 = MaxPooling2D(2)(conv2)  # S / 4
#
#     conv3 = n_conv2d(
#         pool2, 2, dropout_rate, padding="same", filters=filters(256)
#     )  # S / 4
#     pool3 = MaxPooling2D(2)(conv3)  # S / 8
#
#     conv4 = n_conv2d(
#         pool3, 2, dropout_rate, padding="same", filters=filters(512)
#     )  # S / 8
#     pool4 = MaxPooling2D(2)(conv4)  # S / 16
#
#     score_pool4 = Conv2D(filters=n_classes, kernel_size=1)(pool4)  # S / 16
#
#     conv_5 = n_conv2d(
#         pool4, 2, dropout_rate, padding="same", filters=filters(512)
#     )  # S / 16
#     pool_5 = MaxPooling2D(2)(conv_5)  # S / 32
#     fc = n_conv2d(
#         pool_5, 2, dropout_rate, padding="same", filters=filters(4096)
#     )  # S / 32
#     score_fc = Conv2D(filters=n_classes, kernel_size=1)(fc)  # S / 32
#     score_2 = conv2d_transpose_crop(
#         score_fc, n_classes, strides=2, padding="same"
#     )  # S / 16
#
#     add_2 = Add()([score_2, score_pool4])  # S / 16
#     upsample_new = conv2d_transpose_crop(
#         add_2, n_classes, strides=16, padding="same"
#     )  # S
#
#     predictions = Activation(softmax)(upsample_new)
#
#     model = Model(inputs=input_, outputs=predictions)
#     model.compile(
#         optimizer=Adam(lr=lr),
#         loss=sparse_categorical_crossentropy,
#         metrics=[sparse_categorical_accuracy],
#     )
#     return model
