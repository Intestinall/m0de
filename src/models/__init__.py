"""
With S the initial size.
Sources :
    - https://www.researchgate.net/publication/332415241_A_Comparative_study_on_Fully_Convolutional_Networks_FCN-8_FCN-16_and_FCN-32  # noqa
    - https://towardsdatascience.com/review-fcn-semantic-segmentation-eb8c9b50d2d1
    - https://divamgupta.com/image-segmentation/2019/06/06/deep-learning-semantic-segmentation-keras.html  # noqa
    - http://www.deeplearning.net/tutorial/fcn_2D_segm.html
"""
