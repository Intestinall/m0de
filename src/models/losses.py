import tensorflow as tf
import tensorflow.keras.backend as K

from src.models.metrics import dice_coef


@tf.function
def jaccard_distance_loss(y_true, y_pred, smooth=100):
    intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
    sum_ = K.sum(K.abs(y_true) + K.abs(y_pred), axis=-1)
    jac = (intersection + smooth) / (sum_ - intersection + smooth)
    return (1 - jac) * smooth


@tf.function
def dice_loss(y_true, y_pred):
    return 1 - dice_coef(y_true, y_pred)
