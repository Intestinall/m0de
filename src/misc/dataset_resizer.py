from pathlib import Path


def dataset_resizer(length: int, imaterialist_path: Path):
    import csv
    import random
    import shutil
    import sys
    from pathlib import Path

    csv.field_size_limit(sys.maxsize)

    TRAIN_IMAGES = Path(imaterialist_path, "train")
    TRAIN_CSV = Path(imaterialist_path, "train.csv")
    OUTPUT_DATASET = Path("dataset")

    # Select N random images
    randoms_train_images = random.choices(
        tuple(TRAIN_IMAGES.iterdir()), k=length
    )
    images_names_set = {x.name[: -len(".jpg")] for x in randoms_train_images}

    # Remove old images from dataset
    for image in OUTPUT_DATASET.iterdir():
        image.unlink()

    # Copy new image to dataset
    for image in randoms_train_images:
        shutil.copy(image, Path(OUTPUT_DATASET, image.name))

    # Create new train.csv
    with open("train.csv", "w") as train_csv:
        writer = csv.writer(train_csv)
        with open(TRAIN_CSV) as csvfile:
            reader = csv.reader(csvfile)
            writer.writerow(next(reader))
            for image_id, *row in reader:
                if image_id in images_names_set:
                    writer.writerow([image_id, *row])


if __name__ == "__main__":
    dataset_resizer(1, Path(
        Path.home(), "Downloads", "Jdownload", "IMaterialist-fashion-2020"
    ))
