import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from src.misc.colors import COLORS


def limit_gpu_memory_usage(tf_, gigabytes):
    for gpu in tf_.config.experimental.list_physical_devices("GPU"):
        tf_.config.experimental.set_virtual_device_configuration(
            gpu,
            [
                tf_.config.experimental.VirtualDeviceConfiguration(
                    memory_limit=1024 * gigabytes
                )
            ],
        )


def set_memory_growth(tf_):
    for gpu in tf_.config.experimental.list_physical_devices("GPU"):
        tf_.config.experimental.set_memory_growth(gpu, True)


def create_model_architecture(model):
    model.summary()
    tf.keras.utils.plot_model(
        model, to_file="model.png", show_shapes=True, show_layer_names=True
    )


def plot_masks(image: np.ndarray):
    plt.imshow(COLORS[image])
    plt.show()
