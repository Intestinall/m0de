import csv
import itertools
import random
import sys
from pathlib import Path
from typing import Iterator, List, Tuple

import numpy as np

from src.components.image import IMaterialistImage
from src.components.image_class import ImageClass

KERAS_BATCHES = Iterator[Tuple[np.ndarray, np.ndarray]]

csv.field_size_limit(sys.maxsize)


class IMaterialistGenerator:
    def __init__(self, train_csv_path: Path, is_attribute: bool = False) -> None:
        self.train_csv_path = train_csv_path
        self.indexes = self.__get_image_indexes()
        self.is_attribute = is_attribute

    def __get_image_indexes(self) -> Tuple[int, ...]:
        indexes = []
        with self.train_csv_path.open() as csv_file:
            reader = csv.DictReader(csv_file)
            image_id = None
            for i, line_dict in enumerate(reader):
                if line_dict["ImageId"] != image_id:
                    image_id = line_dict["ImageId"]
                    indexes.append(i)
        return tuple(indexes)

    def _get_n_indexes(self, n: int) -> List[int]:
        return random.sample(self.indexes, k=n)

    def _get_image_from_index(self, i: int) -> IMaterialistImage:
        image_classes = []
        with self.train_csv_path.open() as csv_file:
            reader = itertools.islice(csv.DictReader(csv_file), i, None)
            image_id = None
            for line_dict in reader:
                if image_id and line_dict["ImageId"] != image_id:
                    break
                else:
                    image_id = line_dict["ImageId"]
                    image_classes.append(ImageClass(line_dict))
        return IMaterialistImage(image_classes)

    def __call__(
        self, n_targets: int, size: int, batch_size: int, serif: bool = False
    ) -> KERAS_BATCHES:
        while True:
            x_s, y_s = [], []
            for index in self._get_n_indexes(batch_size):
                image = self._get_image_from_index(index)
                x, y = image.x_y_train(
                    n_targets, resize=size, attribute=self.is_attribute, serif=serif
                )
                x_s.append(x)
                y_s.append(y)
            yield np.asarray(x_s), np.asarray(y_s)
            x_s.clear()
            y_s.clear()

    def test_generator(
        self, n_targets: int, size: int, batch_size: int, serif: bool = False
    ) -> KERAS_BATCHES:
        image = self._get_image_from_index(self.indexes[0])
        x, y = image.x_y_train(
            n_targets, resize=size, attribute=self.is_attribute, serif=serif
        )
        while True:
            yield np.asarray([x]), np.asarray([y])
