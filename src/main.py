from pathlib import Path

import tensorflow as tf  # noqa

from src.misc.generator import IMaterialistGenerator
from src.misc.utils import create_model_architecture  # noqa
from src.misc.utils import limit_gpu_memory_usage  # noqa
from src.misc.utils import set_memory_growth  # noqa
from src.models import unet

# limit_gpu_memory_usage(tf, 4)
# set_memory_growth(tf)

N_CLASSES = 46
N_CLASSES_WITH_BACKGROUND = N_CLASSES + 1
N_ATTRIBUTES = 341
N_ATTRIBUTES_WITH_BACKGROUND = N_ATTRIBUTES + 1
IS_ATTRIBUTE = True
N_TARGETS = N_ATTRIBUTES_WITH_BACKGROUND if IS_ATTRIBUTE else N_CLASSES_WITH_BACKGROUND

BATCH_SIZE = 1
SIZE = 512
LR = 1e-4
DROPOUT_RATE = 0.2
MODEL_NAME = "MODEL_TRASH_TERASH.h5"
SERIF = False

if __name__ == "__main__":
    model = unet.main(
        SIZE,
        n_targets=N_TARGETS,
        lr=LR,
        dropout_rate=DROPOUT_RATE,
        is_attribute=IS_ATTRIBUTE,
        serif=SERIF,
    )
    # create_model_architecture(model)
    generator = IMaterialistGenerator(Path("train.csv"), is_attribute=IS_ATTRIBUTE)
    try:
        history = model.fit(
            generator(N_TARGETS, SIZE, BATCH_SIZE, serif=SERIF), epochs=1,
        )
    finally:
        model.save(Path("saved_models", MODEL_NAME))
