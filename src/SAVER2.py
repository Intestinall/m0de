import csv
import random
import sys
from pathlib import Path

import tensorflow as tf  # noqa

from src.misc.generator import IMaterialistGenerator
from src.misc.utils import create_model_architecture  # noqa
from src.misc.utils import limit_gpu_memory_usage  # noqa
from src.misc.utils import set_memory_growth  # noqa
from src.models import unet
csv.field_size_limit(sys.maxsize)

s = set()

with Path("kaggle_submission_TO_EDIT.csv").open() as csv_file:
    reader = csv.DictReader(csv_file)

    for i, line_dict in enumerate(reader):
        s.add(line_dict["ImageId"])

s2 = set()
for file in Path("test").iterdir():
    s2.add(file.name[:-len(".jpg")])

print(len(s2 - s))
with open("kaggle_submission_TO_EDIT.csv", "a") as csvfile:
    for image_id in sorted(s2 - s):
        fieldnames = ["ImageId", "EncodedPixels", "ClassId", "AttributesIds"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        r_p_s = random.randint(1, 1000)
        r_p_o = random.randint(0, 200)
        class_id = random.randint(0, 45)

        writer.writerow({
            "ImageId": image_id,
            "EncodedPixels": f"{r_p_s} {r_p_o}",
            "ClassId": class_id,
            "AttributesIds": ""
        })


s = set()

with Path("kaggle_submission_TO_EDIT.csv").open() as csv_file:
    reader = csv.DictReader(csv_file)

    for i, line_dict in enumerate(reader):
        s.add(line_dict["ImageId"])

s2 = set()
for file in Path("test").iterdir():
    s2.add(file.name[:-len(".jpg")])
print(len(s2 - s))
