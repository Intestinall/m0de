import csv
from pathlib import Path
from typing import Tuple, Dict, List

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import time

TEST_PATH = Path("test")

N_CLASSES = 46
N_CLASSES_WITH_BACKGROUND = N_CLASSES + 1
N_ATTRIBUTES = 341
N_ATTRIBUTES_WITH_BACKGROUND = N_ATTRIBUTES + 1

SIZE = 512
MODEL = tf.keras.models.load_model(Path("saved_models", "MODEL2_class.h5"))
MODEL_ATTR = tf.keras.models.load_model(
    Path(
        "saved_models",
        "MODEL_attributes_537_epochs_7.2267_loss_0.0264_accuracy_2461_seconds.h5"
    )
)


def get_attr(l: List[List[int]]) -> List[int]:
    if not l:
        return []

    max_ = 0
    max_i = 0
    for i, sl in enumerate(l):
        if len(sl) > max_:
            max_ = len(sl)
            max_i = i

    return l[max_i]


def scale(img: np.ndarray, resize: int) -> np.ndarray:
    h, w = img.shape[:2]
    if h > w:
        new_h = resize
        new_w = (resize * w) // h
    else:
        new_h = (resize * h) // w
        new_w = resize
    return cv2.resize(img, (new_w, new_h))


def predict(model, path: Path) -> np.ndarray:
    img = plt.imread(path)
    if len(img.shape) == 2:
        img = np.stack((img,) * 3, axis=-1)
    img = cv2.resize(img, (SIZE, SIZE))
    y: np.ndarray = model.predict(np.asarray([img]))[0]
    return scale(y, SIZE * 2)

st = time.time()

with open("kaggle_submission.csv", "a+") as csvfile:
    fieldnames = ["ImageId", "EncodedPixels", "ClassId", "AttributesIds"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

    for i_image, image_path in enumerate(TEST_PATH.iterdir(), start=1):
        image_id = f"{image_path.name[:-len('.jpg')]}"
        print(
            f"Processing image {image_id} => {i_image}/3200 ({int(time.time() - st)}s)"
        )

        y = np.rot90(predict(MODEL, image_path))
        y_attr = np.rot90(predict(MODEL_ATTR, image_path))
        y = y.reshape(y.shape[0] * y.shape[1], y.shape[2])
        y_attr = y_attr.reshape(y_attr.shape[0] * y_attr.shape[1], y_attr.shape[2])

        last_class_id = int(np.argmax(y[0]))
        last_class_id_position = 0
        occurrences = 0
        last_attrs: List[List[int]] = []

        class_dict: Dict[int, List[Tuple[int, int]]] = {}
        attr_dict: Dict[int, List[int]] = {}

        for i, (c, attr) in enumerate(zip(y, y_attr)):
            class_id = int(np.argmax(c))
            if class_id != last_class_id:
                if class_id not in class_dict:
                    class_dict[class_id] = []
                if class_id not in attr_dict:
                    attr_dict[class_id] = []

                class_dict[class_id].append((last_class_id_position, occurrences))

                a = get_attr(last_attrs)
                if len(a) > len(attr_dict[class_id]):
                    attr_dict[class_id] = a

                last_class_id = class_id
                last_class_id_position = i
                occurrences = 1
                last_attrs.clear()
            else:
                occurrences += 1
                if class_id < N_CLASSES:
                    std_attrs = np.std(attr)
                    attrs = [i for i, x in enumerate(attr) if x >= 0.01 and i < N_ATTRIBUTES]
                    if len(attrs) < 20:
                        attrs.sort()
                    else:
                        attrs = [np.argmax(attr)]

                    if attrs:
                        last_attrs.append(attrs)

        for class_id in class_dict:
            encoded_pixels = " ".join([f"{x[0]} {x[1]}" for x in class_dict[class_id]])
            writer.writerow({
                "ImageId": image_id,
                "EncodedPixels": encoded_pixels,
                "ClassId": class_id,
                "AttributesIds": f"{','.join(str(x) for x in attr_dict[class_id])}"
            })
