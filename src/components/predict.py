from pathlib import Path

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from src.components.image import IMaterialistImage
from src.main import SIZE
from src.misc.utils import plot_masks

MODEL = Path("saved_models", "MODEL2_class.h5")
DATASET = Path("dataset")
IMAGES = [
    Path(p)
    for p in (
        "003d41dd20f271d27219fe7ee6de727d.jpg",
    )
]
# IMAGES = list(DATASET.iterdir())

for image in IMAGES:
    img = plt.imread(image)
    img = cv2.resize(img, (SIZE, SIZE))
    plt.imshow(img)
    plt.show()

    model = tf.keras.models.load_model(MODEL)
    predictions = model.predict(np.expand_dims(img, 0))
    plot_masks(np.argmax(predictions.squeeze(), -1))
