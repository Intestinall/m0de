from typing import Dict, List


class ImageClass:
    def __init__(self, line_dict: Dict[str, str]) -> None:
        self.image_id: str = line_dict["ImageId"]
        self.encoded_pixels: List[str] = line_dict["EncodedPixels"].split()
        self.class_id: int = int(line_dict["ClassId"])
        self.attributes_ids: List[int] = [
            int(x) for x in line_dict["AttributesIds"].split(",") if x
        ]
