from pathlib import Path
from typing import Callable, Iterator, List, Tuple

import cv2
import matplotlib.pyplot as plt
import numpy as np

from src.components.image_class import ImageClass
from src.misc.colors import COLORS


class IMaterialistImage:
    def __init__(self, image_classes: List[ImageClass]) -> None:
        self.image_id: str = image_classes[0].image_id
        self.classes: List[ImageClass] = image_classes

    @staticmethod
    def _iter_encoded_pixels(encoded_pixels: List[str]) -> Iterator[Tuple[int, int]]:
        for i in range(0, len(encoded_pixels), 2):
            yield int(encoded_pixels[i]), int(encoded_pixels[i + 1])

    def _create_y(self, h: int, w: int, n_classes: int) -> np.ndarray:
        def f(seg_labels_, pixel_h, pixel_h_stop, pixel_w, _colors, class_):
            seg_labels_[pixel_h:pixel_h_stop, pixel_w] = class_.class_id

        seg_labels = np.full((h, w), n_classes - 1, dtype=np.uint16)
        self._populate_seglabels(h, seg_labels, f)
        return seg_labels

    def _create_y_attribute(self, h: int, w: int, n_attributes: int) -> np.ndarray:
        def f(seg_labels_, pixel_h, pixel_h_stop, pixel_w, _colors, class_):
            if class_.attributes_ids:
                seg_labels_[pixel_h:pixel_h_stop, pixel_w, class_.attributes_ids] = 1
            else:
                seg_labels_[pixel_h:pixel_h_stop, pixel_w, n_attributes - 1] = 1

        seg_labels = np.zeros((h, w, n_attributes), dtype=np.uint16)
        self._populate_seglabels(h, seg_labels, f)
        return seg_labels

    def display_y_image(self, h: int, w: int, resize: int):
        def f(seg_labels_, pixel_h, pixel_h_stop, pixel_w, colors, class_):
            seg_labels_[pixel_h:pixel_h_stop, pixel_w] = colors[class_.class_id]

        seg_labels = np.zeros((h, w, 3), dtype=np.uint16)
        self._populate_seglabels(h, seg_labels, f)
        seg_labels = self.scale(seg_labels, resize * 2)
        plt.imshow(seg_labels)
        plt.show()

    def _populate_seglabels(
        self, h: int, seg_labels: np.ndarray, seg_labels_updater: Callable
    ):
        for class_ in self.classes:
            for index, occurrences in self._iter_encoded_pixels(class_.encoded_pixels):
                pixel_w, pixel_h = divmod(index, h)
                seg_labels_updater(
                    seg_labels, pixel_h, pixel_h + occurrences, pixel_w, COLORS, class_
                )

    @staticmethod
    def scale(img: np.ndarray, resize: int, old: bool = True) -> np.ndarray:
        h, w = img.shape[:2]
        if old:
            new_h = resize
            new_w = resize
        elif h > w:
            new_h = resize
            new_w = (resize * w) // h
        else:
            new_h = (resize * h) // w
            new_w = resize
        return cv2.resize(img, (new_w, new_h))

    @property
    def path(self) -> Path:
        return Path("dataset", f"{self.image_id}.jpg")

    def x_y_train(
        self,
        n_classes: int,
        resize: int,
        attribute: bool = False,
        serif: bool = True,
        display_y_masks: bool = False,
    ) -> Tuple[np.ndarray, np.ndarray]:
        img = plt.imread(self.path)

        if len(img.shape) == 2:
            img = np.stack((img,) * 3, axis=-1)

        h, w, _ = img.shape

        if display_y_masks:
            self.display_y_image(h, w, resize)

        img = self.scale(img, resize)

        seg_labels = (
            self._create_y_attribute(h, w, n_classes)
            if attribute
            else self._create_y(h, w, n_classes)
        )
        seg_labels = self.scale(seg_labels, resize * 2 if serif else resize)
        return img, seg_labels
